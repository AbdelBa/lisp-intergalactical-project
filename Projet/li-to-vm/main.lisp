(defun make-list-instr (list-instr)
  (progn
    (setf (get list-instr ':main) (vector nil))
    (setf (get list-instr ':compt) 0)
    (setf (get list-instr ':meta-fun) (vector nil))))


(defun add-instr (instr list-instr)
  (print instr))
;  (progn
;    (setf (get list-instr ':main) (cons instr (get list-instr ':main)))
;    (setf (get list-instr ':compt) (+ 1 (get list-instr ':taille)))
;    (print instr)))


(defun li-to-svm (expr &optional (list-instr (vector nil)))
  (case (car expr)
 ;       (:apply (apply-to-svm ()))
 ;       (:sclosure (sclosure-to-svm()))
 ;       (:lclosure (lclosure-to-svm()))
 ;       (:quote (quote-to-svm ()))
        (:if (if-to-svm (cdr expr) list-instr))
 ;       (:progn (progn-to-svm ()))
 ;       (:set-var (set-var-to-svm ()))
 ;       (:set-fun (set-fun-to-svm ()))
        (:call (call-to-svm (cdr expr) list-instr))
 ;       (:mcall (mcall-to-svm ()))
        (:const (const-to-svm (cdr expr) list-instr))
        (:var (var-to-svm (cdr expr) list-instr))
 ;       (:unknown nil)))
))


(defun map-li-to-svm (lexpr &optional (list-instr (vector nil)))
  (mapcar (lambda (expr) (li-to-svm expr list-instr)) lexpr))


;(defun apply-to-svm (args env) nil)
;(defun sclosure-to-svm (args env) nil)
;(defun lclosure-to-svm (args env) nil)
;(defun quote-to-svm (args env) nil)


(defun if-to-svm (expr list-instr)
  (li-to-svm (first expr) list-instr)
  (add-instr `(:SKIPNIL) list-instr)
  ;(let ((pointer (get list-instr ':compt)))
    (li-to-svm (second expr) list-instr)
    ;(let ((jump (- (get list-instr ':compt) pointer)))
      ;(setf (get list-instr ':main) (replace-instr ':TEMP )))
      (li-to-svm (third expr) list-instr))
  



;  (li-to-svm (cadr expr) env
;    )
;(defun progn-to-svm (args env) nil)
;(defun set-var-to-svm (args env) nil)
;(defun set-fun-to-svm (args env) nil)


(defun call-to-svm (expr list-instr)
  (map-li-to-svm (cdr expr) list-instr)
  (add-instr `(:CALL ,(car expr)) list-instr))


;(defun mcall-to-svm (args env) nil)


(defun const-to-svm (expr list-instr)
  (add-instr `(:CONST ,expr) list-instr))


(defun var-to-svm (expr list-instr)
  (add-instr `(:VAR ,expr) list-instr))


;(defun unknown-to-svm (args env) nil)
