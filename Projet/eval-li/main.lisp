;;;; Où l'on traite de l'évaluation proprement dite.
;;;; À chaque cas représenté par une étiquette on associe une fonction d'évaluation.
;;;; L'intérêt c'est de pouvoir facilement modifier chaque traitement sans modifier l'analyse par cas.

;;; Analyse par cas.

(require 'dyn-env "dyn-env.lisp")

(defun eval-li (expr &optional (env (vector nil)))
  (case (car expr)
        (:quote (quote-eval-li (cdr expr)))
        (:const (const-eval-li (cdr expr)))
        (:var (var-eval-li (cdr expr) env))
        (:set-var (set-var-eval-li (cadr expr) env (caddr expr)))
        (:set-fun (set-fun-eval-li expr env))
        (:if (if-eval-li (cadr expr) (caddr expr) (cadddr expr) env))
        (:call (call-eval-li (cadr expr) (cddr expr) env))
        (:mcall (mcall-eval-li (cadr expr) (cddr expr) env))
        (:unknown (unknown-eval-li expr env))
        (:progn (progn-eval-li (cdr expr) env))
        (:apply (case (caadr expr)
                      (:lclosure (lapply-eval-li (cadr expr) (cddr expr) env))
                      (:sclosure (sapply-eval-li (cadr expr) (cddr expr) env))))
        (:closurize (closurize-meta-fun (cdr expr) env))))

(defun map-eval-li (lexpr env)
  (mapcar (lambda (expr) (eval-li expr env)) lexpr))

;;; Évaluation

(defun quote-eval-li (expr)
  `',expr)

(defun const-eval-li (expr)
  expr)

(defun var-eval-li (pos env)
  (get-var pos env))

(defun set-var-eval-li (pos env val)
  (set-var pos env (eval-li val env)))

(defun set-fun-eval-li ()
  ())

(defun if-eval-li (condition true false env)
  (if (eval-li condition env)
      (eval-li true env)
      (eval-li false env)))

(defun call-eval-li (fun args env)
  (eval `(funcall #',fun ,@(map-eval-li args env))))

(defun mcall-eval-li (fun args env)
  (eval-li (get-defun fun) 
           (if (get fun 'to-closurize)
               (include-env (make-dyn-env (map-eval-li args env))
                            (get fun 'closed-env))
               (include-env (make-dyn-env (map-eval-li args env))
                            env))))

(defun unknown-eval-li (expr env)
  (let ((li-expr (lisp-to-li (cadr expr) (caddr expr))))
    (if (eql :unknown (car li-expr))
        (error "Fonction ~s inconnue" (cadr li-expr))
        (eval-li li-expr env))))

(defun progn-eval-li (lexpr env)
  (loop for expr in (butlast lexpr) do
        (eval-li expr env))
  (eval-li (car (last lexpr)) env))

(defun lapply-eval-li (closure args env)
  (eval-li (caddr closure)
           (include-env (make-dyn-env (map-eval-li args env) (cadr closure)) env)))

(defun sapply-eval-li (closure args env)
  (eval `(funcall ,(cadr closure) ,@(map-eval-li args env))))

(defun closurize-meta-fun (fun env)
  (setf (get fun 'closed-env) env)
  nil)