;;;; Où l'on traite des environnements dynamiques de leur création et de leur manipulation.

(defun make-dyn-env (args &optional (env-size (length args)))
  "Produit un environnement, c.à.d un vecteur avec la premiere case vide et les arguments après."
  (make-array (list (1+ env-size)) :initial-contents (cons nil args)))

(defun include-env (sub-env sup-env)
  "Ajoute un pointeur dans la 1ière case de l'environnement englobé vers l'environnement englobant (capture)."
  (setf (aref sub-env 0) sup-env)
  sub-env)

(defun get-var (pos env)
  "La position est de type (<profondeur> . <position>) dans l'environnement."
  (if (= 0 (car pos))
      (aref env (cdr pos))
      (get-var (cons (1- (car pos)) (cdr pos))
               (aref env 0))))

(defun set-var (pos env val)
  "Affecte la valeur donnée à la position donnée dans l'environnement donné et retourne val pour faire classe."
  (if (= 0 (car pos))
      (setf (aref env (cdr pos)) val)
      (set-var (cons (1- (car pos)) (cdr pos))
               (aref env 0)
               val))
  val)

(provide 'dyn-env)