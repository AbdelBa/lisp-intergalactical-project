(defun load-lisp-to-li ()
    (load "lisp-to-li/stat-env.lisp")
    (load "lisp-to-li/meta-def.lisp")
    (load "lisp-to-li/main.lisp"))

(defun load-eval-li ()
    (load "eval-li/dyn-env.lisp")
    (load "eval-li/main.lisp"))

(defun load-li-to-vm ()
    (load "li-to-vm/main.lisp"))

(defun load-stack-vm ()
    (load "stack-vm/main.lisp"))

(defun load-all ()
    (load-lisp-to-li)
    (load-eval-li)
    (load-li-to-vm)
    (load-stack-vm))