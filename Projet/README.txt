Avant toute chose charger le chargeur avec la ligne:

(load "loader.lisp")

Une fois que c'est fait vous aurez accès aux fonctions suivantes

    • (load-lisp-to-li)
    • (load-eval-li)
    • (load-li-to-vm )
    • (load-stack-vm)
    • (load-all)

Qui font exactement ce qu'on imagine qu'elles font, c'est à dire charger les différents modules ou les charger tous (et dans les ténèbres les lier).

Ensuite il vous suffit d'ouvrir le fichier de démonstration nommé "demo.lisp" et piocher au gré de la guise de vos envies les portions de code à exécuter.