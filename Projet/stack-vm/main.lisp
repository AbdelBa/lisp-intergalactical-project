;;;; La machine virtuelle à pile.


(defun make-vm (vm &key (name "Simple-VM") (memory-size 5000))

  (setf (get vm 'name) name
	(get vm 'memory) (make-array memory-size)
        (get vm 'htrr) (make-hash-table) ; Les Références rŽsolues.
        (get vm 'htra) (make-hash-table) ; Les Références en avant.
        (get vm 'last-load-addr) (1- memory-size))
  ;; Initialisation de la pile
  (set-value vm 0 3) ;Stack Pointer
  (set-value vm 1 3) ;Frame Pointer
  (set-value vm 2 (1- memory-size)) ;Code Pointer
  (set-value vm 3 (1- memory-size)) ;Counter Code
  )


;;; CHARGEMENT de la VM
(defun load-vm (code &key vm)
  "Charge une liste d'instructions dans la machine virtuelle."
  (let ((ghtrr (get vm 'htrr))
        (ghtra (get vm 'htra))
        (lhtrr (make-hash-table))
        (lhtra (make-hash-table))
        (last-addr (get vm 'last-load-addr)))
    (format t "Start loading ...~%")
    ;; Une jolie analyse par cas.

    (loop for instr in code do
          (case (car instr)
                (:label
                  (if (global-labelp (cdr instr))
                      (label-case vm last-addr (cdr instr) ghtrr ghtra)
                      (label-case vm last-addr (cdr instr) lhtrr lhtra)))
                (:jmp
                  (if (global-labelp (cdr instr))
                      (jmp-case vm last-addr instr ghtrr ghtra)
                      (jmp-case vm last-addr instr lhtrr lhtra)))
                (otherwise
                 (load-instruction vm instr))))

    ;; Vérification de la table des références en avant voir s'il en reste.
    (loop for label being the hash-keys of lhtra using (hash-value addr) do
          (format t "Références non rŽsolue (label, address) = (~S, ~S)" label addr))))


(defun label-case (vm addr label htrr htra)
  (if (not (in-tab label htrr))
      (progn
        (format t "Traitement du label ~S~%" label)
        (solve-forward-references vm label addr htra)
        (remhash label htra)
        (setf (gethash label htrr) addr)
        t)
      (progn
        (warn "Ambiguïté, redéfinition de label ~s." label) ; Penser à enregistrer l'erreur qqpart.
        nil)))

(defun solve-forward-references (vm label addr htra)
  (loop for fwref in (gethash label htra) do
        (setf (aref (get vm 'memory) fwref) addr)))


(defun jmp-case (vm last-addr instr htrr htra)
  (progn
    (if (in-tab (cdr instr) htrr)
        (setf instr (cons :jmp last-addr))
        (setf (gethash (cdr instr) htra)
              (cons (gethash (cdr instr) htra) last-addr)))
    (load-instruction vm instr)))

  

(defun global-labelp (label)
  "Par convention un symbole représente une étiquette globale (toute la vm)."
  (symbolp label))

(defun local-labelp (label)
  "Par convention un entier représente une étiquette globale (unité de code)."
  (integerp label))


(defun load-instruction (vm instr)
  (let ((addr (get vm 'last-load-addr)))    
    (setf (aref (get vm 'memory) addr) instr ; L'instruction est écrite à la dernire adresse dispo.
          (get vm 'last-load-addr) (1- addr))) ; Décrémentation du pointeur d'adresse. 
   (set-CC vm (1- (get-CC vm)))) ; Décrémentation du pointeur d'adresse.


(defun in-tab (key hashtable)
  (not (eql (gethash key hashtable) nil)))


(defun run-vm (&key vm)
  "Lance la machine  virtuelle à la dernière adresse chargée."
  (loop while (>  (get-CP vm) (get-CC vm))
     do
       (run-instructions vm (get-value vm (get-CP vm)))
       (set-CP vm (1- (get-CP vm)))))
     
(defun vm-apply (fn vm &rest args)
  "Equivalent de apply qui applique la fonction fn préalablement chargée."
  ())



(defun run-instructions (vm expr)
  (case (car expr)
    (:CONST (const-instr vm expr))
    (:VAR (var-instr vm expr))
    (:SET-VAR  (set-var-instr vm expr))
    (:STACK (stack-instr vm expr))
    (:CALL (call-instr vm expr))
    (:RTN (rtn-instr vm expr))
    (:SKIP (skip-instr vm expr))
    (:SKIPNIL (skipnil-instr vm expr))
    (:SKIPTRUE (skiptrue-instr vm expr))
    (t (error "Erreur run-instructions ~s" expr ))
    ))


(defun const-instr (vm expr)
  (set-SP vm (+ 1 (get-SP vm))) ; incrémente le SP
  (set-value vm (get-SP vm) (cadr expr)) ; empile la valeur
  (print ":CONST a été empilé"))

(defun var-instr (vm expr))

(defun set-var-instr (vm expr)) 

(defun stack-instr (vm expr))

(defun call-instr (vm expr)
  (let ((arg1 (get-value vm (get-SP vm)))
	(arg2 (get-value vm (1- (get-SP vm)))))
    (let ((result (apply (cadr expr) arg1 arg2 '() )))
      (set-value vm (1- (get-SP vm)) result))) ; On empile le resultat

   (set-SP vm (get-FP vm))
   (print ":call terminé")) ; décrémente le SP

(defun rtn-instr (vm expr))

(defun skip-instr (vm expr))

(defun skipnil-instr (vm expr))    

(defun skiptrue-instr (vm expr))


;; " ---- Primitives for stack  ---- " ;;
(defun set-value (vm index value)
  " Set value to stack index" 
  (setf (aref (get-stack vm) index) value))

(defun get-value (vm index)
  "Get value at index given"
  (aref (get-stack vm) index))


(defun get-stack (vm)
  "Get memory"
  (get vm 'memory)) 

(defun get-FP (vm)
  "Get Frame Pointer (FP)"
  (aref (get vm 'memory) 1))

(defun get-SP (vm)

  "Get Stack Pointer (SP)"
  (aref (get vm 'memory) 0))

(defun get-CC (vm)
  "Get Code Counter (SP)"
  (aref (get vm 'memory) 3))

(defun get-CP (vm)
  "Get Counter Pointer (CP)"
  (aref (get vm 'memory) 2))

(defun set-SP (vm value)
  "Set CP)"
  (setf (aref (get vm 'memory) 0) value ))

(defun set-CP (vm value)
  " Set CP "
  (setf (aref (get vm 'memory) 2) value))

(defun set-CC (vm value)
  "Set CP)"
  (setf (aref (get vm 'memory) 3) value ))




