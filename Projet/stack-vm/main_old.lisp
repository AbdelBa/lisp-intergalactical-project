(defun demo (expr)
  (let ((li-expr (lisp-to-li expr)))
    (format t "~S ~% Eval : ~S" li-expr (eval-li li-expr))))

;; If
(demo '(if (> 4 5) "Flute" "Chocolat"))

;; Expression lambda.
(demo '((lambda (y i) (* y (- i 1))) 8 7))

;; Valeur fonctionnelle lambda.
(demo '(lambda (y i) (* y (- i 1))))

;; Let-expression.
(demo '(let ((n 3) (j 9)) (+ n j) (* j n)))

;; Defun
(demo '(defun fibo (n)
         (if (<= n 1)
             n
             (+ (fibo (- n 1)) (fibo (- n 2))))))

;; Defun avec lambda-capture.
(demo '(defun lambda-fact (n)
         ((lambda (x) (if (<= x 1)
                          x
                          (* (lambda-fact (- x 1)) x))) n)))

;; Apply fonction prédéfinie.
(demo '(apply #'+ 1 2 3 '(4 5)))

(demo '(let ((v 1)) (apply (lambda (u) (+ u v)) '(7))))
      
;; Apply lambda-expression.
(demo '(apply (lambda (u) (+ u 1)) '(7)))

;; Appel fonction prédéfinie.
(demo '(car '(6 7 8)))

;; Appel fonction méta-définie
(demo '(lambda-fact 5))

;; Exemple du compteur
(demo '(let ((n 0))
         (defun counter () n)
         (defun counter++ () (setf n (1+ n)))
         (defun counter-reset () (setf n 0))))

(demo '(counter))
(demo '(progn (counter++) (counter++) (counter++)))
(demo '(counter))
(demo '(counter-reset))
(demo '(counter))

;; Fonction complexe.
(demo '(defun progn-to-li (args env)
         (if (atom args)
             (atom-to-li args env)
             (if (= (length args) 1)
                 (lisp-to-li (car args) env)
                 `(:progn ,@(map-lisp-to-li args env))))))
                    