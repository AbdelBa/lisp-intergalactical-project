;;;; Petite démonstration, c'est pas les moines shaolin mais c'est quand même impressionnant.

;;; Partie lisp-to-li et eval-li

(defun demo (expr)
  "Affiche la version en langage intermédiaire de l'expression et son évaluation "
  (let ((li-expr (lisp-to-li expr)))
    (format t "~S ~% Eval : ~S" li-expr (eval-li li-expr))))

;; If
(demo '(if (> 4 5) "Flute" "Chocolat"))

;; Expression lambda.
(demo '((lambda (y i) (* y (- i 1))) 8 7))

;; Valeur fonctionnelle lambda.
(demo '(lambda (y i) (* y (- i 1))))

;; Let-expression.
(demo '(let ((n 3) (j 9)) (+ n j) (* j n)))

;; Defun de fibo
(demo '(defun fibo (n)
         (if (<= n 1)
             1
             (+ (fibo (- n 1)) (fibo (- n 2))))))

(demo '(fibo 4))
(demo '(fibo (fibo 4)))
(demo '(fibo (fibo (fibo 4)))) ; Ne pas remplacer 4 par 5.

;; Defun avec lambda-capture.
(demo '(defun lambda-fact (n)
         ((lambda (x) (if (<= x 1)
                          x
                          (* (lambda-fact (- x 1)) x))) n)))

;; Apply fonction prédéfinie.
(demo '(apply #'+ 1 2 3 '(4 5)))

(demo '(let ((v 1)) (apply (lambda (u) (+ u v)) '(7))))
      
;; Apply lambda-expression.
(demo '(apply (lambda (u) (+ u 1)) '(7)))

;; Appel fonction prédéfinie.
(demo '(car '(6 7 8)))

;; Appel fonction méta-définie
(demo '(lambda-fact 5))

;; Exemple du compteur
(demo '(let ((n 0))
         (defun counter () n)
         (defun counter++ () (setf n (1+ n)))
         (defun counter-reset () (setf n 0))))

(demo '(counter))
(demo '(progn (counter++) (counter++) (counter++)))
(demo '(counter))
(demo '(counter-reset))
(demo '(counter))

;; Fonction complexe.
(demo '(defun progn-to-li (args env)
         (if (atom args)
             (atom-to-li args env)
             (if (= (length args) 1)
                 (lisp-to-li (car args) env)
                 `(:progn ,@(map-lisp-to-li args env))))))


;;; Partie li-to-vm et stack-vm

;; Création de la VM
(make-vm 'toto :memory-size 100)

;; Transformation simple en langage vm
(let* ((code-li (lisp-to-li '(+ 1 2)))
      (code-vm (li-to-svm code-li))))

;; Chargement du code en mémoire
(load-vm '((:CONST 1) (:CONST 4) (:CALL +)) :vm 'toto)

;; Un petit aperçu de la mémoire
(get 'toto 'memory)

;; Exécution de la fonction chargée
(run-vm :vm 'toto)
(get 'toto 'memory) ; pour visualisé la mémoire après appel de la fonction

(get-SP vm) ; Sommet de pile mis à jour
(get-value vm (get-SP vm)) ;résultat de la fonction
(get-CP vm) ; Le pointeur de code déplacé

                    