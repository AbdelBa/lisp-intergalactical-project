;;;; Où l'on traite de la transformation proprement dite.
;;;; On isole précisément chaque cas. 
;;;; L'intérêt est de pouvoir facilement modifier chaque cas sans toucher au code d'analyse par cas.

;;; Analyse par cas.

(require 'meta-def "meta-def.lisp")

(defun lisp-to-li (expr &optional (env (vector nil))) 
  (if (atom expr)
      (atom-to-li expr env)
      (cons-to-li expr env)))


(defun map-lisp-to-li (lexpr &optional (env (vector nil)))
  (mapcar (lambda (expr) (lisp-to-li expr env)) lexpr))

(defun map-lisp-to-li* (lexpr &optional (env (vector nil)))
  (append (map-lisp-to-li (butlast lexpr) env)
          (map-lisp-to-li (cadar (last lexpr)) env))) ;Prend en compte le quote pas le list.

(defun atom-to-li (expr env)
  (if (constantp expr)
      (const-to-li expr)
      (let ((pos (var-pos expr env))) ; Pour ne pas faire la recherche 2 fois.
        (if (not (null pos))
            (var-to-li pos)
            (error "Variable inconnue : ~s" expr)))))


(defun cons-to-li (expr env)
  (if (consp (car expr))
      (if (eq 'lambda (caar expr))
          (lambda-to-li (car expr) (cdr expr) env)
          (error "Une expression evaluable ne commence pas par (( : ~s" expr))
      (if (symbolp (car expr))
          (if (or (is-meta-fun (car expr)) (fboundp (car expr)))
              (if (special-form-p (car expr))
                  (special-form-to-li expr env)
                  (if (macro-function (car expr))
                      (lisp-to-li (macroexpand-1 expr) env)
                      (if (is-meta-fun (car expr))
                          (if (enough-args (car expr) (length (cdr expr)))
                              (mfunction-to-li (car expr) (cdr expr) env)
                              (error "Mauvais nombre d'arguments dans l'appel à ~s" (car expr)))
                          (if (eq 'apply (car expr))
                              (apply-to-li (cdr expr) env)
                              (pfunction-to-li (car expr) (cdr expr) env)))))
              (unknown-to-li expr env))
          (error "~s n'est pas un symbole" (car expr)))))

(defun special-form-to-li (expr env)
  (case (car expr)
        (if (if-to-li (cdr expr) env))
        (quote (quote-to-li (cadr expr)))
        (setf (setf-to-li (cdr expr) (make-setf-env env (cdr expr))))
        (function (function-to-li expr env))
        (defun (set-defun (cadr expr) (caddr expr) (cdddr expr) env))
        (labels "Not yet implemented")
        (let (let-to-li (cadr expr) (cddr expr) env))
        (progn (progn-to-li (cdr expr) env))
        (block "Not yet implemented")
        (otherwise (error "Forme spéciale inconnue: ~s" (car expr)))))

;;; Transformations.

(defun function-to-li (expr env)
  (if (symbolp expr) `#',expr)
  (if (symbolp (cadr expr))
      (if (is-meta-fun (car expr))
          (lambda-closure-to-li (get (car expr) 'nb-params-tot) 
                                (get (car expr) 'code))           
          `(:sclosure ,expr))
      (if (eq 'lambda (caadr expr))
      (lambda-closure-to-li (cadadr expr)  (car (cddadr expr)) env))))
      

(defun apply-to-li (args env)
  `(:apply ,@(map-lisp-to-li* args env)))

(defun lambda-closure-to-li (params body env)
  `(:lclosure 
     ,(length params) 
     ,(lisp-to-li body (include-env (make-stat-env params) env))))


(defun lambda-to-li (body args env)
  `(:apply
     ,(lambda-closure-to-li (cadr body) (caddr body) env)
     ,@(map-lisp-to-li args env)))

(defun let-to-li (bindings bodies env)
  "On réécrit le let en lambda-expression, ça mange pas de main."
  (lisp-to-li
   `((lambda
       ,(loop for pair in bindings collect (car pair))
       (progn ,@bodies))
     ,@(loop for pair in bindings collect (cadr pair)))
   env))

(defun quote-to-li (expr)
  `(:quote . ,expr))

(defun if-to-li (args env)
  `(:if 
     ,(lisp-to-li (car args) env)
     ,(lisp-to-li (cadr args) env)
     ,(lisp-to-li (caddr args) env)))


(defun setf-to-li (args env)
  `(:progn ,@(loop for place in args by #'cddr
                   for value in (cdr args) by #'cddr
                   collect (if (symbolp place)
                               (set-var-to-li place value env)
                               (set-fun-to-li place value env)))))


(defun set-var-to-li (var value env)
  "(:set-var (<env-distance> <env-indice>) <valeur>)"
  `(:set-var ,(var-pos var env) ,(lisp-to-li value env)))

(defun set-fun-to-li (place value env)
  "(:set-fun <fonction> <argument>* <valeur>)"
  `(:set-fun ,(car place) ,(map-lisp-to-li (cdr place) env) ,(lisp-to-li value env)))

(defun progn-to-li (args env)
  (if (atom args)
      (atom-to-li args env)
      (if (= (length args) 1)
          (lisp-to-li (car args) env)
          `(:progn ,@(map-lisp-to-li args env)))))

(defun pfunction-to-li (fun args env)
  `(:call ,fun ,@(map-lisp-to-li args env)))

(defun mfunction-to-li (fun args env)
  `(:mcall ,fun ,@(map-lisp-to-li args env)))


(defun const-to-li (expr)
  `(:const . ,expr))

(defun var-to-li (pos)
  `(:var  ,@pos))

(defun unknown-to-li (expr env)
  `(:unknown ,expr ,env))

;;; Utilitaires.

(defun special-form-p (fun)
  "Pas forcément présente dans toutes les distributions lisp (parfois nommée special-operator-p)"
  (find fun '(if quote setf function defun block catch declare           ; Ces formes spéciales  
                 eval-when flet go labels let let* macrolet            ; peuvent être trouvées
                 multiple-value-call multiple-value-prog1 progn progv  ; dans la doc CLisp
                 return-from setq tagbody the throw unwind-protect))) 

(provide 'lisp-to-li)
