;;;; Où l'on traite des environnements statiques de leur création et de leur manipulation.

(defun make-stat-env (params)
  "Produit un environnement, c.à.d un vecteur avec la premiere case vide et les paramètres après."
  (make-array (list (1+ (length params))) :initial-contents (cons nil params)))


(defun make-setf-env (env setf-args)
  "Agrandit l'environnent autant que nécessaire pour accueillir les nouvelles variables éventuelles."
  (loop for place in setf-args by #'cddr when (symbolp place) do
        (if (not (in-env place env))
            (setf env (concatenate 'vector env (vector place)))))
  env)

(defun in-env (var env)
  "Retourne vrai si la variable var se trouve dans l'environnement env ou dans ceux qui l'englobent, faux sinon."
  (if (not (null env))
      (or (not (null (position var env)))
          (in-env var (aref env 0)))))

(defun var-pos (var env)
  "Renvoie une paire d'entiers de type (<profondeur> . <position>) , nil si var n'apparait pas."
  (labels ((var-posl (var env depth)
             (if (not (null env))
                 (if (not (null (position var env))) ; Trouvé.
                     (cons depth (position var env))
                     (var-posl var (aref env 0) (1+ depth)))
                 nil)))
    (var-posl var env 0)))

(defun include-env (sub-env sup-env)
  "Ajoute un pointeur dans la 1ière case de l'environnement englobé vers l'environnement englobant (capture)."
  (setf (aref sub-env 0) sup-env)
  sub-env)

(provide 'stat-env)