;;;; Où l'on traite des définitions de fonctions des accès à leurs attributs.
;;;; La stratégie consiste à enregistrer toutes les informations utiles dans la liste des attributs.

(require 'stat-env "stat-env")

(defun set-defun (symb params code env)
  "Intègre dans le symbole d'une fonction tout les éléments nécessaires au traitement de ladite fonction."
  (let ((clean-params (sanitize params)))  ; Nettoyage des paramètres     
    (setf (get symb 'is-meta-fun) t
          ;; Si l'environnement est non-vide on trandformera le defun en fermeture à l'évaluation.
          (get symb 'to-closurize) (> (length env) 1)
          (get symb 'has-rest-param) (not (null (position '&rest params)))
          ;; On enregistre le nombre de paramètres obligatoires.
          (get symb 'nb-params-oblig) (find-if #'integerp (list (position '&optional params) ; Quelques paramètres obligatoires.
                                                                (position '&rest params)     ; Tous sauf le dernier.
                                                                (length params)))            ; Tous obligatoires.
          ;; On enregistre le nombre de paramètres total.
          (get symb 'nb-params-tot) (length params)
          ;; On transforme le code avec un environnement propre.
          (get symb 'code) (lisp-to-li (add-opt-code params (remove-docstring code))
                                       (include-env (make-stat-env clean-params) env))))
  (if (get symb 'to-closurize)
      `(:closurize . ,symb)
      '()))

(defun get-defun (symb)
  (get symb 'code))

(defun is-meta-fun (symb)
  (get symb 'is-meta-fun))

(defun enough-args (fun nb-args)
  "Pour être en règle il faut plus d'arguments que le nombre minimal obligatoire et moins que
   le nombre de paramètres optionnel + obligatoire à moins d'avoir un paramètre &rest."
  (if (<= (get fun 'nb-params-oblig) nb-args)
      (or (get fun 'has-rest-param ) (>= (get fun 'nb-params-tot) nb-args))))


(defun add-opt-code (params code)
  "Ajoute le code de vérification des paramètres optionnels avec valeur par défaut au code de base."
  `(progn ,@(opt-check-code (slice-opt params)) ,@code))

(defun remove-docstring (code)
  "Le code représente le corps d'une fonction, enlève la chaine de documentation si présente."
  (if (and (stringp (car code)) (not (null (cdr code)))) ; Ce n'est pas une fonction constante.
      (cdr code)
      code))

(defun slice-opt (params)
  "Renvoie la sous-liste de parametres optionnels, éventuellement vide."
  (cdr (member '&optional params)))

(defun opt-check-code (opt-params)
  "Gènère le code nécessaire à la vérification des paramètres optionnels avant d'entrer dans le code de la fonction."
  (if (not (endp opt-params))
      (if (consp (car opt-params)) ; Paramètre avec valeur par défaut.
          (cons `(if (null ,(caar opt-params)) (setf ,(caar opt-params) ,(cadar opt-params)))
                (opt-check-code (cdr opt-params)))
          (opt-check-code (cdr opt-params)))))

(defun sanitize (params)
  "Nettoie la liste de paramètres en enlevant les &rest et &optional et en applatissant la liste (r 5) -> r."
  (if (not (endp params))
      (if (consp (car params))
          (cons (caar params) (sanitize (cdr params)))
          (if (find (car params) '(&optional &rest))
              (sanitize (cdr params))
              (cons (car params) (sanitize (cdr params)))))))

(provide 'meta-def)